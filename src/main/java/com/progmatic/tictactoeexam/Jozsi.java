/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.interfaces.Board;

/**
 *
 * @author User
 */
public class Jozsi extends com.progmatic.tictactoeexam.interfaces.AbstractPlayer{
    

    public Jozsi(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        Cell c;
        if(b.emptyCells().size()>0){
            c=b.emptyCells().get(0);
            c.setCellsPlayer(myType);
            return c;
        }
        return null;
    }
    
}
