/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Gyulus extends com.progmatic.tictactoeexam.interfaces.AbstractPlayer {

    public Gyulus(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        Cell c = null;
        
        try {
            List<Cell[]> list = new ArrayList<>();
            PlayerType bum;
            Cell[] a0 = new Cell[3];
            bum = b.getCell(2, 0);
            a0[0] = new Cell(2, 0, bum);
            bum = b.getCell(1, 1);
            a0[1] = new Cell(1, 1, bum);
            bum = b.getCell(0, 2);
            a0[2] = new Cell(0, 2, bum);
            list.add(a0);

            Cell[] a1 = new Cell[3];
            bum = b.getCell(0, 0);
            a1[0] = new Cell(0, 0, bum);
            bum = b.getCell(0, 1);
            a1[1] = new Cell(0, 1, bum);
            bum = b.getCell(0, 2);
            a1[2] = new Cell(0, 2, bum);
            list.add(a1);

            Cell[] a2 = new Cell[3];
            bum = b.getCell(1, 0);
            a2[0] = new Cell(1, 0, bum);
            bum = b.getCell(1, 1);
            a2[1] = new Cell(1, 1, bum);
            bum = b.getCell(1, 2);
            a2[2] = new Cell(1, 2, bum);
            list.add(a2);

            Cell[] a3 = new Cell[3];
            bum = b.getCell(2, 0);
            a3[0] = new Cell(2, 0, bum);
            bum = b.getCell(2, 1);
            a3[1] = new Cell(2, 1, bum);
            bum = b.getCell(2, 2);
            a3[2] = new Cell(2, 2, bum);
            list.add(a3);

            Cell[] a4 = new Cell[3];
            bum = b.getCell(0, 0);
            a4[0] = new Cell(0, 0, bum);
            bum = b.getCell(1, 0);
            a4[1] = new Cell(1, 0, bum);
            bum = b.getCell(2, 0);
            a4[2] = new Cell(2, 0, bum);
            list.add(a4);

            Cell[] a5 = new Cell[3];
            bum = b.getCell(0, 1);
            a5[0] = new Cell(0, 1, bum);
            bum = b.getCell(1, 1);
            a5[1] = new Cell(1, 1, bum);
            bum = b.getCell(2, 1);
            a5[2] = new Cell(2, 1, bum);
            list.add(a5);

            Cell[] a6 = new Cell[3];
            bum = b.getCell(0, 2);
            a6[0] = new Cell(0, 2, bum);
            bum = b.getCell(1, 2);
            a6[1] = new Cell(1, 2, bum);
            bum = b.getCell(2, 2);
            a6[2] = new Cell(2, 2, bum);
            list.add(a6);

            Cell[] a7 = new Cell[3];
            bum = b.getCell(0, 0);
            a7[0] = new Cell(0, 0, bum);
            bum = b.getCell(1, 1);
            a7[1] = new Cell(1, 1, bum);
            bum = b.getCell(2, 2);
            a7[2] = new Cell(2, 2, bum);
            list.add(a7);

            boolean thisisgood = false;
            int row;
            int col;
            for (int i = 0; i < list.size(); i++) {
                thisisgood = check(list.get(i));
                if (thisisgood == true) {
                    for (int j = 0; j < 3; j++) {
                        if (list.get(i)[j].getCellsPlayer().equals(PlayerType.EMPTY)) {
                            c=list.get(i)[j];
                            c.setCellsPlayer(myType);
                            break;
                        }
                    }
                }
            }
            
//            if(thisisgood==false){
//                List <Cell> empty =new ArrayList<>();
//                empty=b.emptyCells();
//                c=empty.get(0);
//                
//            }
            return c;

        } catch (CellException ex) {

        }
        return c;
    }

    private boolean check(Cell[] a) {
        boolean good = false;
        int countempty = 0;
        int countmytype = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i].getCellsPlayer().equals(myType)) {
                countmytype++;
            }
            if (a[i].getCellsPlayer().equals(PlayerType.EMPTY)) {
                countempty++;
            }
        }
        if (countempty == 1 && countmytype == 2) {
            good = true;
        }
        return good;
    }

}
